package control;

public class Conversor {

	private final static Conversor singleton = new Conversor();
	
	private Conversor() {}
	
	public static Conversor getSingleton() {
		return singleton;
	}
	
	/**
	 * Método que recebe uma String representando um número binário
	 * e retorna um inteiro resultado de sua conversão para decinal
	 * 
	 * @param binary
	 * @return
	 */
	public String convetBinToDec(String binary) {
		//Convertendo a variável String para um array de caracteres
		char[] binArray = binary.toCharArray();

		//Declaração de variáveis auxiliares
		int dec = 0;
		int count = 1;
		
		//Iterando ao contrário para conversão a cada casa binária
		for (int i = binArray.length - 1; i >= 0; i--) {
			//Considerando somente os bits ativos para a soma
			if (binArray[i] == '1')
				dec += count;
			//Atualizando o valor em decimal referente à casa binári aatual
			count+=count;
		}
		
		return ""+dec;
	}
	
	/**
	 * Método que recebe uma String representando um número decinal
	 * e retorna um inteiro resultado de sua conversão para binário
	 * 
	 * @param decimal
	 * @return
	 */
	public String convetDecToBin(String decimal) {
		//Convertendo a variável String para decimal
		int dec = Integer.parseInt(decimal);
		String bin = "";
		
		//Capturar o resto da divisão enquanto for possível
		while (dec > 0) {
			int resto = dec % 2;
			//Atualizando o valor para a próxima divisão
			dec /= 2;
			
			//Montando o formato da representação Binária
			bin = resto + bin;
		}
		
		return bin;
	}

}
